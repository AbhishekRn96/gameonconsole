import React, { Component } from 'react';
import {withRouter} from 'react-router-dom';
import config from "../../config";
import { Digital } from 'react-activity';
import 'react-activity/dist/react-activity.css';

class ViewTournament extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tournament : null,
      gameId : '',
      message : '',
      title : '',
      error : null,
      status : null,
      messageerr : null, 
    }
    this.deleteTournament = this.deleteTournament.bind(this);
    this.closeTournament = this.closeTournament.bind(this);
    this.openTournament = this.openTournament.bind(this);
    this.addGameId = this.addGameId.bind(this);
    this.route = this.route.bind(this);
    this.sendMessage = this.sendMessage.bind(this);
    this.getTournament = this.getTournament.bind(this);
  }

  componentWillMount() {
    let TournamentData = this.props.location.state;
    this.getTournament(TournamentData.TournamentId);
    this.setState({
      gameId : TournamentData.GameRoomId,
    })
  }

  getTournament(TournamentId) {
    fetch(`${config.url}getTournament`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        TournamentId : TournamentId,
      }),
    })
    .then(response =>  response.json())
    .then(resData => {
      if(resData !== null) {
        this.setState({tournament : resData});
      }
    })
    .catch(err => {
      console.log(err);
    });
  }

  sendMessage(Tournament) {
    fetch(`${config.url}postMessage`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        Topic : Tournament.TournamentId,
        Arn : Tournament.Topic.TopicArn,
        Message : this.state.message,
        Title : this.state.title,
      }),
    })
    .then(response =>  response.json())
    .then(resData => {
      if(resData.message === "success") {
        this.setState({messageerr : "success"}, () => {
          setTimeout(() => this.setState({messageerr : null}), 3000);
        })
      } else {
        this.setState({messageerr : "failed"}, () => {
          setTimeout(() => this.setState({messageerr : null}), 3000);
        })
      }
    })
    .catch(err => {
      console.log(err);
      this.setState({messageerr : "failed"}, () => {
        setTimeout(() => this.setState({messageerr : null}), 3000);
      })
    });
  }

  addGameId(TournamentId) {
    fetch(`${config.url}addGameId`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        TournamentId : TournamentId,
        GameRoomId : this.state.gameId,
      }),
    })
    .then(response =>  response.json())
    .then(resData => {
      if(resData.message === "success") {
        this.setState({error : "success"}, () => {
          setTimeout(() => this.setState({error : null}), 3000);
        })
      } else {
        this.setState({error : "failed"}, () => {
          setTimeout(() => this.setState({error : null}), 3000);
        })
      }
    })
    .catch(err => {
      console.log(err);
      this.setState({error : "failed"}, () => {
        setTimeout(() => this.setState({error : null}), 3000);
      })
    });
  }

  deleteTournament(Tournament) {
    fetch(`${config.url}removeTournament`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        TournamentId : Tournament.TournamentId,
        Topic : Tournament.Topic.TopicArn
      }),
    })
    .then(response =>  response.json())
    .then(resData => {
      if(resData.message === "deleted") {
        this.props.history.replace({
          pathname : '/tournaments',
        });
      }
    })
    .catch(err => console.log(err));
  }

  closeTournament(TournamentId) {
    fetch(`${config.url}closeTournament`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        TournamentId : TournamentId,
      }),
    })
    .then(response =>  response.json())
    .then(resData => {
      if(resData.message === "success") {
        this.setState({status : "success"}, () => {
          setTimeout(() => this.setState({error : null}), 3000);
        })
      } else {
        this.setState({status : "failed"}, () => {
          setTimeout(() => this.setState({error : null}), 3000);
        })
      }
    })
    .catch(err => {
      console.log(err);
      this.setState({status : "failed"}, () => {
        setTimeout(() => this.setState({error : null}), 3000);
      })
    });
  }

  openTournament(TournamentId) {
    fetch(`${config.url}openTournament`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        TournamentId : TournamentId,
      }),
    })
    .then(response =>  response.json())
    .then(resData => {
      if(resData.message === "success") {
        this.setState({status : "success"}, () => {
          setTimeout(() => this.setState({status : null}), 3000);
        })
      } else {
        this.setState({status : "failed"}, () => {
          setTimeout(() => this.setState({status : null}), 3000);
        })
      }
    })
    .catch(err => {
      console.log(err);
      this.setState({status : "failed"}, () => {
        setTimeout(() => this.setState({status : null}), 3000);
      })
    });
  }
  
  route(item) {
    this.props.history.push({
      pathname : '/showPlayers',
      state : item
    });
  }

  render() {
    let TournamentData = this.state.tournament;
    return(
      this.state.tournament !== null ? (
        <body className='container' style={{marginTop:100}}>
          <div>
            <div 
              style={{
                bottom: 0,
                right: 0,
                position: 'fixed',
                marginRight: '5%',
                marginBottom: '5%'
              }}>
              <button
                onClick={() => this.route(TournamentData)}
                style={{marginTop: '3%'}}
                className='btn btn-primary'>Show Players</button>
              {TournamentData.TournamentStatus === "Open" ? (
                <div>
                  <button
                    onClick={() => this.closeTournament(TournamentData.TournamentId)}
                    style={{marginTop: '3%'}}
                    className='btn btn-danger'>Close Tournament</button>
                </div>
              ) : (
                <div>
                  <button
                    onClick={() => this.openTournament(TournamentData.TournamentId)}
                    style={{marginTop: '3%'}}
                    className='btn btn-success'>Open Tournament</button>
                </div>
              )}
              <div>
                <button
                  onClick={() => {
                    this.props.history.push({
                      pathname : '/editTournament',
                      state : TournamentData
                    });
                  }}
                  style={{marginTop: '3%'}}
                  className='btn btn-primary'>Edit Tournament</button>
              </div>
              <div>
                <button
                  onClick={() => this.deleteTournament(TournamentData)}
                  style={{marginTop: '3%'}}
                  className='btn btn-danger'>Remove Tournament</button>
              </div>
            </div>
          </div>
          {this.state.status === "failed" ? (
            <div className="alert alert-danger alert-dismissible fade show" role="alert">
              Error occured while changing Tournament Status
              <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
          ) : this.state.status === "success" ? (
            <div className="alert alert-success alert-dismissible fade show" role="alert">
              Tournament Status changed successfully
              <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
          ) : null}
          <div style={{marginTop: 40, marginBottom : 40}}>
            <img 
              src={TournamentData.Poster}
              alt='main'
              style={{height: 300, borderRadius: 3}}/>
            <h3 style={{fontWeight: '600', color: '#58595b', marginTop: '2%'}}>{TournamentData.Name} ({TournamentData.TournamentId})</h3>
            <div style={{width : '100%'}}>
              <div style={{padding : 20}}>
                <h5>Publish Message</h5>
                <div>
                <input type="text" className="form-control" placeholder="Enter Title" onChange={(event) => this.setState({title : event.target.value})} />
                  <textarea type="text" className="form-control" row={4} placeholder="Enter Message" onChange={(event) => this.setState({message : event.target.value})} style={{marginTop: '2%'}} />
                  <button disabled={this.state.message && this.state.title ? false : true} className='btn btn-info' onClick={() => this.sendMessage(TournamentData)} style={{marginTop: '2%'}}>Send</button>
                </div>
                {this.state.messageerr === "failed" ? (
                  <p className="text-danger" style={{fontWeight: '500'}}>Some error occured</p>
                ) : this.state.messageerr === "success" ? (
                  <p className="text-success" style={{fontWeight: '500'}}>Message Published successfully</p>
                ) : null}
              </div>
              <div style={{padding : 20}}>
                <h5>Game Room Id</h5>
                <div className="row">
                  <input type="text" className="form-control" value={this.state.gameId} placeholder="Enter Game Room Id" style={{width: '75%', marginLeft: '1.3%'}} onChange={(event) => this.setState({gameId : event.target.value})} />
                  <button disabled={this.state.gameId ? false : true} className='btn btn-info' onClick={() => this.addGameId(TournamentData.TournamentId)} style={{marginLeft: '2%'}}>Save</button>
                </div>
                {this.state.error === "failed" ? (
                  <p className="text-danger" style={{fontWeight: '500'}}>Some error occured</p>
                ) : this.state.error === "success" ? (
                  <p className="text-success" style={{fontWeight: '500'}}>GameRoomId set successfully</p>
                ) : null}
              </div>
              <h5>Description</h5>
              <p>{TournamentData.Desc}</p>
              <h5>Tournament Details</h5>
              <p style={{color : 'grey', fontWeight: '500', fontSize: 18}}>
                <span>
                  <i className='material-icons' style={{color: '#4e6387', marginRight: '0.5%'}}>query_builder</i>
                </span>&nbsp;
                {TournamentData.Hours+':'+TournamentData.Minutes}
              </p>
              <p>Slots - {TournamentData.Slots}</p>
              <p style={{color: 'green'}}>Fee - &#8377;{TournamentData.Fee}</p>
              <p>Starting Interval - {TournamentData.End} Minutes</p>
            </div>
          </div>
        </body>
      ) : (
        <body className='container' style={{marginTop:100}}>
          <center>
            <div style={{marginTop: '20%'}}>
              <Digital color="#1C8ADB" size={50} speed={1} animating={true} />
            </div>
          </center>
        </body>
      )
    )
  }
}

export default withRouter(ViewTournament);
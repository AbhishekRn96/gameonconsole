import React, { Component } from 'react';
import './style.css';
import { Spinner  } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import {withRouter} from 'react-router-dom';
import config from "../../config";

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username : null,
      password : null,
      show : false,
    }
    this.Login = this.Login.bind(this);
  }

  componentWillMount() {
    if(sessionStorage.getItem('user')){
      this.props.history.push('cafes');
    }
  }

  Login(e) {
    this.setState({show : true})
    e.preventDefault();
    fetch(`${config.url}loginAdmin`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        username: this.state.username,
        password: this.state.password,
      }),
    })
    .then(response =>  response.json())
    .then(resData => {
      console.log(resData);
      if(resData.message !== 'Error') {
        sessionStorage.setItem('user', resData.Admin);
        this.props.history.push('cafes');
      } else {
        this.setState({show : false})
      }
    })
    .catch(err => {
      console.log(err);
      this.setState({show : false})
    });
  }

  render() {
    return(
      <div className='container-fluid' style={{marginTop:130}}>
        <center>
          <div className='card col-md-4 shadow center' style={{borderWidth: 2,}}>
            <div className='card-body'>
            <h4 className="headings" style={{color: '#1C8ADB'}}>Hello Admin</h4>
              <form className="form-group">
                <div>
                  <label></label>
                  <input type="text" placeholder="Username" className="form-control" required onChange={(event) => this.setState({username : event.target.value})} />
                </div>
                <div>
                  <label></label>
                  <div>
                    <input type="password" placeholder="Password" className="form-control" required onChange={(event) => this.setState({password : event.target.value})} />
                  </div>
                </div>
                <div style={{marginTop: 30}}>
                  {this.state.show ? (
                    <div style={{marginTop: 20}}>
                      <Spinner  color="#397fef" size={25} speed={1} animating={true} />
                    </div>
                  ) : (
                    <button className="btn-primary button-LS" disabled={this.state.username && this.state.password ? false : true} onClick={this.Login} style={{backgroundColor: '#1C8ADB', color: 'white', fontWeight: '500'}}>
                      LOGIN
                    </button>
                  )}
                </div>
              </form>
            </div>
          </div>
        </center>
      </div>
    )
  }
}

export default withRouter(Home);
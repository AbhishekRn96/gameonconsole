import React, { Component } from 'react';
import './style.css';
import { Dots } from 'react-activity';
import 'react-activity/dist/react-activity.css';
import {withRouter} from 'react-router-dom';
import config from "../../config";

class Cafes extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data : [],
      show: true,
    }
    this.route = this.route.bind(this);
  }

  componentWillMount() {
    fetch(`${config.url}getCafes`)
    .then(response =>  response.json())
    .then(resData => {
      if(resData.length !== 0) {
        this.setState({show : false})
        this.setState({data : resData})
      } else if(resData.length === 0) {
        this.setState({show : "no_cafes"})
      }
    })
    .catch(err => console.log(err));
  }

  route(item) {
    this.props.history.push({
      pathname : '/viewCafe',
      state : item
    });
  }

  render() {
    return (
      <body className='container-fluid' style={{marginTop:100}}>
        {this.state.show === true ? (
          <center style={{marginTop: 250}}>
            <Dots color="#397fef" size={38} speed={1} animating={true} />
          </center>
        ) : this.state.show === "no_cafes" ? (
          <center>
            <div className="text-primary" style={{marginTop: '20%', fontSize: 20, fontWeight: '500'}}>No Cafes registered yet!</div>
          </center>
        ) : (<div></div>)}
        <div>
          {this.state.data.map((item, key) => (
            <div className='card shadow col-md-12' key={key} style={{marginBottom: 5, marginLeft: 10, marginRight: 5}} onClick={() => this.route(item)}>
              <div className='card-body col-md-12'>
                <div className='row'>
                  <img 
                    src={item.Main}
                    alt='main'
                    style={{height: 100, marginLeft: 10, borderRadius: 3}}/>
                  <div style={{marginLeft : 12}}>
                    <h6 className='heading'>{item.Name} ({item.CafeId})</h6>
                    <div className='opens-closes'>
                      <i className='material-icons' style={{color: '#4e6387'}}>person</i> <span className='time'>{item.Vendor}</span>
                    </div>
                    <div>
                    <i className='material-icons' style={{color: 'red'}}>location_on</i> <span className='time'>{item.Location}</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          ))}
        </div>
      </body>
    )
  }
}

export default withRouter(Cafes);
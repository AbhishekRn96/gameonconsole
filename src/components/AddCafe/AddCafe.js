import React, { Component } from 'react';
import './style.css';
import Select from 'react-select';
import {withRouter} from 'react-router-dom';
import config from "../../config";

class AddCafe extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Name : ' ',
      Vendor : ' ',
      Main : ' ',
      Slides : ' ',
      TotalImages : ' ',
      S3Folder : ' ',
      ImageExtension : ' ',
      Desc : ' ',
      OpenTime : ' ',
      CloseTime : ' ',
      Location : ' ',
      CostPC : '',
      CostConsole : '',
      Latitude : ' ',
      Longitude : ' ',
      Address : ' ',
      BankName : ' ',
      AccountNo: ' ',
      IFSCcode: ' ',
      Email: ' ',
      Contactno1: ' ',
      Contactno2: ' ',
      Games: ' ',
      data: [],
      PCs: 0,
      Consoles: 0,
      disable: false,
    }
    this.addCafe = this.addCafe.bind(this);
  }

  componentWillMount() {
    fetch(`${config.url}getGames`)
    .then(response =>  response.json())
    .then(resData => {
      if(resData.length !== 0) {
        this.setState({data : resData})
        let arr = [];
        this.state.data.map((item, key) => arr.push({value : item.Game, label: item.Game, url: item.Url}))
        this.setState({data : arr})
      }
    })
    .catch(err => console.log(err));
  }

  handleSlides(event) {
    let arr = [];
    for(let i=1;i<=this.state.TotalImages;i++) {
      arr.push(this.state.S3Folder+'/'+i+event.target.value)
    }
    this.setState({
      Slides : arr,
      Main : arr[0],
      ImageExtension : event.target.value,
    })
  }

  handleOptions(event) {
    this.setState({
      Games : event
    })
  }

  addCafe() {
    this.setState({disable : true});
    let arr = [];
    let arr2 = [];
    if(this.state.PCs > 0) {
      for(let i = 1; i <= this.state.PCs; i++) {
        arr.push(i);
      }
    }
    if(this.state.Consoles > 0) {
      for(let i = 1; i <= this.state.Consoles; i++) {
        arr2.push(i);
      }
    }
    fetch(`${config.url}dataUpload`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        Name : this.state.Name,
        Vendor : this.state.Vendor,
        Email: this.state.Email,
        Contact: {Contact_1: this.state.Contactno1, Contact_2: this.state.Contactno2},
        BankDetails: {Bank: this.state.BankName, Account: this.state.AccountNo, IFSC: this.state.IFSCcode},
        Main : this.state.Main,
        Slides : this.state.Slides,
        S3FolderDetails : {S3Folder : this.state.S3Folder, TotalImages : this.state.TotalImages, ImageExtension : this.state.ImageExtension},
        Desc : this.state.Desc,
        OpenTime : Number(this.state.OpenTime),
        CloseTime : Number(this.state.CloseTime),
        PCs: this.state.PCs,
        PC_nos: arr,
        Consoles: this.state.Consoles,
        Console_nos: arr2, 
        Location : this.state.Location,
        Address : this.state.Address,
        CostPC : Number(this.state.CostPC),
        CostConsole : Number(this.state.CostConsole),
        Latitude : Number(this.state.Latitude),
        Longitude : Number(this.state.Longitude),
        Games: this.state.Games,
        Rating: 0,
      }),
    })
    .then(response =>  response.json())
    .then(resData => {
      this.setState({disable : false});
      this.props.history.push('/cafes');
    })
    .catch(err => {
      this.setState({disable : false});
      console.log(err);
    });
  }

  render() {
    return (
      <body className='container' style={{marginTop:100}}>
        <h3 style={{fontWeight: '500', color: 'grey'}}>Add a new Cafe</h3>
        <form className='form-group' name='formCheck' onSubmit={this.addCafe}>
          <div className='placement'>
            <label>Cafe Name</label>
            <input type='text' required className='form-control' onChange={(event) => this.setState({Name : event.target.value})} />
          </div>
          <div className='placement'>
            <label>Vendor Name</label>
            <input type='text' required className='form-control' onChange={(event) => this.setState({Vendor : event.target.value})} />
          </div>
          <div className='placement'>
            <label>Email Address</label>
            <input type='email' required className='form-control' onChange={(event) => this.setState({Email : event.target.value})} />
          </div>
          <div className='placement'>
            <div className='placement'>
              <label>Contact Info</label>
            </div>
            <span>
              <input type='text' required pattern='[0-9]{10}' onInvalid={(event) => event.target.setCustomValidity('Contact number should be of 10 digits')} onInput={(event) => event.target.setCustomValidity("")} placeholder='Mobile Number 1' className='form-control-responsive' onChange={(event) => this.setState({Contactno1 : '+91'+event.target.value})} />
            </span>
            <span style={{marginLeft: '2%', marginRight: '2%'}}>   </span>
            <span>
              <input type='text' pattern='[0-9]{10}' onInvalid={(event) => event.target.setCustomValidity('Contact number should be of 10 digits')} onInput={(event) => event.target.setCustomValidity("")} placeholder='Mobile Number 2' className='form-control-responsive' onChange={(event) => this.setState({Contactno2 : '+91'+event.target.value})} />
            </span>
          </div>
          <div className='placement'>
            <label>Bank Name</label>
            <input type='text' required className='form-control' onChange={(event) => this.setState({BankName : event.target.value})} />
          </div>
          <div className='placement'>
            <div className='placement'>
              <label>Bank Account Details</label>
            </div>
            <span>
              <input type='number' required placeholder='Account Number' className='form-control-responsive' onChange={(event) => this.setState({AccountNo : event.target.value})} />
            </span>
            <span style={{marginLeft: '2%', marginRight: '2%'}}>   </span>
            <span>
              <input type='text' required placeholder='IFSC Code' className='form-control-responsive' onChange={(event) => this.setState({IFSCcode : event.target.value})} />
            </span>
          </div>
          <div className='placement'>
            <label>Description</label>
            <textarea className='form-control' required onChange={(event) => this.setState({Desc : event.target.value})} />
          </div>
          <div className='placement'>
            <div className='placement'>
              <label>Timings</label>
            </div>
            <span>
              <input type='number' required placeholder='From' step='0.5' className='form-control-responsive' onChange={(event) => this.setState({OpenTime : event.target.value})} />
            </span>
            <span style={{marginLeft: '2%', marginRight: '2%'}}> - </span>
            <span>
              <input type='number' required placeholder='To' step='0.5' className='form-control-responsive' onChange={(event) => this.setState({CloseTime : event.target.value})} />
            </span>
          </div>
          <div className='placement'>
            <div className='placement'>
              <label>Available Systems</label>
            </div>
            <span>
              <input type='number' placeholder='No of PCs' className='form-control-responsive' onChange={(event) => this.setState({PCs : event.target.value})} />
            </span>
            <span style={{marginLeft: '2%', marginRight: '2%'}}>   </span>
            <span>
              <input type='number' placeholder='No of Consoles' className='form-control-responsive' onChange={(event) => this.setState({Consoles : event.target.value})} />
            </span>
          </div>
          <div className='placement'>
            <label>Cost per Hour</label>
            <div>
              PC : &nbsp;&#8377;<input type='number' required placeholder='Cost per hour' className='form-control-responsive' onChange={(event) => this.setState({CostPC : event.target.value})} value={this.state.CostPC} />
            </div>
            <span style={{marginLeft: '2%', marginRight: '2%'}}>   </span>
            <div>
              Consoles : &nbsp;&#8377;<input type='number' required placeholder='Cost per hour' className='form-control-responsive' onChange={(event) => this.setState({CostConsole : event.target.value})} value={this.state.CostConsole} />
            </div>
          </div>
          <div className='placement'>
            <label>Address</label>
            <textarea type='text' required className='form-control' onChange={(event) => this.setState({Address : event.target.value})} />
          </div>
          <div className='placement'>
            <label>Location</label>
            <input type='text' required className='form-control' onChange={(event) => this.setState({Location : event.target.value})} />
          </div>
          <div className='placement'>
            <div>
              <label>Mapview Co-ordinates</label>
            </div>
            <span>
              <input type='text' required pattern='-?\d{1,3}\.\d+' onInvalid={(event) => event.target.setCustomValidity('Please enter a valid Latitude value')} onInput={(event) => event.target.setCustomValidity("")} placeholder='Latitude' className='form-control-responsive' onChange={(event) => this.setState({Latitude : event.target.value})} />
            </span>
            <span style={{marginLeft: '2%', marginRight: '2%'}}> - </span>
            <span>
              <input type='text' required pattern='-?\d{1,3}\.\d+' onInvalid={(event) => event.target.setCustomValidity('Please enter a valid Longitude value')} onInput={(event) => event.target.setCustomValidity("")} placeholder='Longitude' className='form-control-responsive' onChange={(event) => this.setState({Longitude : event.target.value})} />
            </span>
          </div>
          <div className='placement'>
            <div>
              <label>S3 Folder Name</label>
            </div>
            <span>
              <input type='text' required className='form-control-responsive' onChange={(event) => this.setState({S3Folder : event.target.value})} />
            </span>
          </div>
          <div className='placement'>
            <label>No of Images</label>
            <div>
              <input type='number' required className='form-control-responsive' onChange={(event) => this.setState({TotalImages : event.target.value})}  />
            </div>
          </div>
          <div className='placement'>
            <label>Image Extension</label>
            <div>
              <select className='form-control-responsive' required onChange={this.handleSlides.bind(this)} >
                <option value='.jpeg'>.jpeg</option>
                <option value='.jpg'>.jpg</option>
                <option value='.png'>.png</option>
              </select>
            </div>
          </div>
          <div className='placement'>
            <label>Select Games</label>
            <Select
              required
              options={this.state.data}
              isMulti
              isSearchable
              isClearable
              onChange={this.handleOptions.bind(this)}/>
          </div>
          <div className='placement'>
            <input type="submit" disabled={this.state.disable} value="Add" required className='btn btn-info' />
          </div>
        </form>
      </body>
    )
  }
}

export default withRouter(AddCafe);
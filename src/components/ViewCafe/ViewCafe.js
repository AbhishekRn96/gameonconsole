import React, { Component } from 'react';
import './style.css';
import {withRouter} from 'react-router-dom';
import config from "../../config";

class ViewCafe extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data : [],
    }
    this.deleteCafe = this.deleteCafe.bind(this);
  }

  deleteCafe(CafeId) {
    fetch(`${config.url}removeCafe`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        CafeId : CafeId,
      }),
    })
    .then(response =>  response.json())
    .then(resData => {
      this.props.history.replace({
        pathname : '/cafes',
      });
    })
    .catch(err => console.log(err));
  }

  render() {
    const CafeData = this.props.location.state;
    return(
      <body className='container' style={{marginTop:100}}>
        <div>
          <div 
            style={{
              bottom: 0,
              right: 0,
              position: 'fixed',
              marginRight: '5%',
              marginBottom: '5%'
            }}>
            <div>
              <button
                onClick={() => {
                  this.props.history.push({
                    pathname : '/viewDates',
                    state : CafeData
                  });
                }}
                className='btn btn-info'>View Dates</button>
            </div>
            <div>
              <button
                onClick={() => {
                  this.props.history.push({
                    pathname : '/editCafe',
                    state : CafeData
                  });
                }}
                style={{marginTop: '3%'}}
                className='btn btn-primary'>Edit Cafe</button>
            </div>
            <div>
              <button
                onClick={() => this.deleteCafe(CafeData.CafeId)}
                style={{marginTop: '3%'}}
                className='btn btn-danger'>Remove Cafe</button>
            </div>
          </div>
          <div id="carouselExampleIndicators" className="carousel slide sizer" data-ride="carousel">
            <ol className="carousel-indicators">
              <li data-target="#carouselExampleIndicators" data-slide-to="0" className="active"></li>
              <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
              <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div className="carousel-inner">
              <div className="carousel-item active">
                <img className="d-block w-100" src={CafeData.Main} alt="First slide" />
              </div>
              <div className="carousel-item">
                <img className="d-block w-100" src={CafeData.Main} alt="Second slide" />
              </div>
              <div className="carousel-item">
                <img className="d-block w-100" src={CafeData.Main} alt="Third slide" />
              </div>
            </div>
            <a className="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
              <span className="carousel-control-prev-icon" aria-hidden="true"></span>
              <span className="sr-only">Previous</span>
            </a>
            <a className="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
              <span className="carousel-control-next-icon" aria-hidden="true"></span>
              <span className="sr-only">Next</span>
            </a>
          </div>
        </div>
        <div style={{marginTop: 40}}>
          <h3 style={{fontWeight: '600', color: '#58595b'}}>{CafeData.Name} ({CafeData.CafeId})</h3>
          <div className='content'>
            <h5>Cafe Owner and Contact details</h5>
            <h6>{CafeData.Vendor}</h6>
            <p>Email - {CafeData.Email}</p>
            <p>Phone - {CafeData.Contact.Contact_1}, {CafeData.Contact.Contact_2}</p>
            <h5>Bank details</h5>
            <p>Bank - {CafeData.BankDetails.Bank}</p>
            <p>Account number - {CafeData.BankDetails.Account}</p>
            <p>IFSC number - {CafeData.BankDetails.IFSC}</p>
            <h5>Description</h5>
            <p>{CafeData.Desc}</p>
            <h5>Address</h5>
            <p>{CafeData.Address}</p>
            <h5>Pricing</h5>
            <p>PC - {'\u20B9'}{CafeData.CostPC}, Console - {'\u20B9'}{CafeData.CostConsole}</p>
          </div>
        </div>
      </body>
    )
  }
}

export default withRouter(ViewCafe);
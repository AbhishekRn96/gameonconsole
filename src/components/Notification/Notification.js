import React, { Component } from 'react';
import {withRouter} from 'react-router-dom';
import config from "../../config";
import { Digital } from 'react-activity';
import 'react-activity/dist/react-activity.css';

class Notification extends Component {
  constructor(props) {
    super(props);
    this.state = {
      message1 : '',
      message2 : '',
      title : '',
      err1 : '',
      err2 : '',
    }
    this.sendMessage = this.sendMessage.bind(this);
    this.sendNoti = this.sendNoti.bind(this);
  }

  sendMessage() {
    fetch(`${config.url}promoSMS`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        Message : this.state.message1,
      }),
    })
    .then(response =>  response.json())
    .then(resData => {
      if(resData.message === "success") {
        this.setState({messageerr : "success"}, () => {
          setTimeout(() => this.setState({err1 : ''}), 3000);
        })
      } else {
        this.setState({messageerr : "failed"}, () => {
          setTimeout(() => this.setState({err1 : ''}), 3000);
        })
      }
    })
    .catch(err => {
      console.log(err);
      this.setState({messageerr : "failed"}, () => {
        setTimeout(() => this.setState({err1 : ''}), 3000);
      })
    });
  }

  sendNoti() {
    fetch(`${config.url}promoNoti`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        Title : this.state.title,
        Message : this.state.message2,
      }),
    })
    .then(response =>  response.json())
    .then(resData => {
      if(resData.message === "success") {
        this.setState({messageerr : "success"}, () => {
          setTimeout(() => this.setState({err1 : ''}), 3000);
        })
      } else {
        this.setState({messageerr : "failed"}, () => {
          setTimeout(() => this.setState({err1 : ''}), 3000);
        })
      }
    })
    .catch(err => {
      console.log(err);
      this.setState({messageerr : "failed"}, () => {
        setTimeout(() => this.setState({err1 : ''}), 3000);
      })
    });
  }

  render() {
    return(
      <body className='container' style={{marginTop:100}}>
        <div style={{padding : 20}}>
          <h5>Publish Message</h5>
          <div>
            <textarea type="text" className="form-control" row={4} placeholder="Enter Message" onChange={(event) => this.setState({message1 : event.target.value})} />
            <button disabled={this.state.message1 ? false : true} onClick={() => this.sendMessage()} className='btn btn-info' style={{marginTop: '2%'}}>Send</button>
          </div>
          {this.state.err1 !== "success" && this.state.err1 !== '' ? (
            <p className="text-danger" style={{fontWeight: '500'}}>Some error occured</p>
          ) : this.state.err1 === "success" ? (
            <p className="text-success" style={{fontWeight: '500'}}>Message Published successfully</p>
          ) : null}
        </div>
        <div style={{padding : 20}}>
          <h5>Publish Notification</h5>
          <div>
            <input type="text" className="form-control" placeholder="Enter Title" onChange={(event) => this.setState({title : event.target.value})} />
            <textarea type="text" className="form-control" row={4} placeholder="Enter Message" onChange={(event) => this.setState({message2 : event.target.value})} style={{marginTop: '2%'}} />
            <button disabled={this.state.message2 && this.state.title ? false : true} onClick={() => this.sendNoti()} className='btn btn-info' style={{marginTop: '2%'}}>Send</button>
          </div>
          {this.state.err2 === "failed" ? (
            <p className="text-danger" style={{fontWeight: '500'}}>Some error occured</p>
          ) : this.state.err2 === "success" ? (
            <p className="text-success" style={{fontWeight: '500'}}>Message Published successfully</p>
          ) : null}
        </div>
      </body>
    )
  }
}

export default withRouter(Notification);
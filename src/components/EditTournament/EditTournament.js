import React, { Component } from 'react';
import {withRouter} from 'react-router-dom';
import config from "../../config";

class EditTournament extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Name : '',
      Desc : '',
      Date : '',
      Hours : '',
      Minutes : '',
      Poster : '',
      Slots : '',
      Fee : '',
      End : '',
      TournamentId : '',
      WinAmount : '',
      SlotsAvailable : '',
      TournamentStatus : '',
      Topic : {},
      Game : {},
      disable: false,
    }
    this.goBack = this.goBack.bind(this);
    this.editTournament = this.editTournament.bind(this);
  }

  async componentWillMount() {
    let TournamentData = this.props.location.state;
    console.log(TournamentData);
    await this.setState({
      Game : TournamentData.Game,
      Name : TournamentData.Name,
      Desc : TournamentData.Desc,
      Date : TournamentData.Date,
      Hours : TournamentData.Hours,
      Minutes : TournamentData.Minutes,
      Poster : TournamentData.Poster,
      Slots : TournamentData.Slots,
      Fee : TournamentData.Fee,
      End : TournamentData.End,
      WinAmount : TournamentData.WinAmount,
      TournamentId : TournamentData.TournamentId,
      SlotsAvailable : TournamentData.SlotsAvailable,
      TournamentStatus : TournamentData.TournamentStatus,
      Topic : TournamentData.Topic,
    })
  }

  editTournament() {
    this.setState({disable : true});
    fetch(`${config.url}dataUpdateT`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        Game : this.state.Game,
        Name : this.state.Name,
        Desc : this.state.Desc,
        Date : this.state.Date,
        Hours : this.state.Hours,
        Minutes : this.state.Minutes,
        Poster : this.state.Poster,
        Slots : this.state.Slots,
        Fee : this.state.Fee,
        End : this.state.End,
        WinAmount : this.state.WinAmount,
        TournamentId : this.state.TournamentId,
        SlotsAvailable : this.state.SlotsAvailable,
        TournamentStatus : this.state.TournamentStatus,
        Topic : this.state.Topic,
      }),
    })
    .then(response =>  response.json())
    .then(resData => {
      if(resData.message === "updated") {
        this.setState({disable : false});
        this.props.history.push('/tournaments');
        // this.goBack();
      }
    })
    .catch(err => {
      this.setState({disable : false});
      console.log(err);
    });
  }

  goBack(){
    this.props.history.goBack();
  }

  render() {
    return(
      <body className='container' style={{marginTop: 120, marginLeft: 30}}>
        <h3 style={{fontWeight: '500', color: 'grey'}}>Edit Cafe Details</h3>
        <form className='form-group' name='formCheck'>
          <div className='placement'>
            <label>Tournament Name</label>
            <input type='text' required className='form-control' onChange={(event) => this.setState({Name : event.target.value})} value={this.state.Name} />
          </div>
          <div className='placement'>
            <label>Tournament Description</label>
            <textarea row={4} required className='form-control' onChange={(event) => this.setState({Desc : event.target.value})} value={this.state.Desc} />
          </div>
          <div className='placement'>
            <label>Winning Amount</label>
            <input type='number' required className='form-control' onChange={(event) => this.setState({WinAmount : event.target.value})} />
          </div>
          <div className='placement'>
            <label>Tournament Date</label>
            <input type='date' required className='form-control' onChange={(event) => this.setState({Date : event.target.value})} value={this.state.Date} />
          </div>
          <div className='placement'>
            <label>Tournament Time</label>
            <div className="form-row">
              <div className="col">
                <input type="number" required min={0} max={24} step={1} className="form-control" placeholder="Hours - 24:00 format"  onChange={(event) => this.setState({Hours : event.target.value})} value={this.state.Hours} />
              </div>
              <div className="col">
                <input type="number" required min={0} max={59} step={1} className="form-control" placeholder="Minutes" onChange={(event) => this.setState({Minutes : event.target.value})} value={this.state.Minutes} />
              </div>
            </div>
          </div>
          <div className='placement'>
            <label>S3 Image/Poster Name (jpeg | png | jpg)</label>
            <input type='text' required className='form-control' placeholder="eg : pubg.png" pattern="[^\s]+(\.(jpg|jpeg|png))$" onChange={(event) => this.setState({Poster : event.target.value})} value={this.state.Poster} />
          </div>
          <div className='placement'>
            <label>Total Slots</label>
            <input type='number' required className='form-control' onChange={(event) => this.setState({Slots : event.target.value})} value={this.state.Slots} />
          </div>
          <div className='placement'>
            <label>Entry Fee</label>
            <input type='number' required className='form-control' onChange={(event) => this.setState({Fee : event.target.value})} value={this.state.Fee} />
          </div>
          <div className='placement'>
            <label>Registration Close Interval in Minutes</label>
            <input type='number' required className='form-control' onChange={(event) => this.setState({End : event.target.value})} value={this.state.End} />
          </div>
        </form>
        <div className='placement' style={{marginBottom: '5%'}}>
          <span>
            <button required disabled={this.state.disable} onClick={() => this.editTournament()} className='btn btn-info'>Save Changes</button>
          </span>
        </div>
      </body>
    )
  }
}

export default withRouter(EditTournament);
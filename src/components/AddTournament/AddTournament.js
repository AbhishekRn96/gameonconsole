import React, { Component } from 'react';
import './styles.css'
import Select from 'react-select';
import { Link, withRouter } from 'react-router-dom';
import config from "../../config";

class AddTournament extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Name : '',
      Desc : '',
      Date : '',
      Hours : '',
      Minutes : '',
      Game: '',
      Poster : '',
      Slots : '',
      Fee : '',
      End : '',
      WinAmount : '',
      disable : false,
      alert : null,

    }
    this.createTournament = this.createTournament.bind(this);
  }

  componentWillMount() {
    fetch(`${config.url}getGames`)
    .then(response =>  response.json())
    .then(resData => {
      if(resData.length !== 0) {
        this.setState({data : resData});
      }
    })
    .catch(err => console.log(err));
  }

  createTournament(e) {
    e.preventDefault();
    fetch(`${config.url}createTournament`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        Game : this.state.Game,
        Name : this.state.Name,
        Desc : this.state.Desc,
        Date : this.state.Date,
        Hours : this.state.Hours,
        Minutes : this.state.Minutes,
        Poster : this.state.Poster,
        Slots : this.state.Slots,
        Fee : this.state.Fee,
        End : this.state.End,
        WinAmount : this.state.WinAmount,
      }),
    })
    .then(response => response.json())
    .then(resData => {
      if(resData.n === 1 && resData.ok === 1) {
        this.setState({alert : true})
        setTimeout(() => this.setState({alert : null}), 3000);
        this.props.history.push('/tournaments');
      }
    })
    .catch(err => console.log(err));
  }

  handleOptions(event) {
    this.setState({
      Game : event
    })
  }

  render() {
    return (
      <body className='container' style={{marginTop:100}}>
        <h3 style={{fontWeight: '500', color: 'grey'}}>Add a new Cafe</h3>
        <form className='form-group' name='formCheck' onSubmit={this.createTournament}>
          <div className='placement'>
            <label>Select Game</label>
            <Select
              required
              options={this.state.data}
              isSingle
              isSearchable
              isClearable
              onChange={this.handleOptions.bind(this)}/>
          </div>
          <div className='placement'>
            <label>Tournament Name</label>
            <input type='text' required className='form-control' onChange={(event) => this.setState({Name : event.target.value})} />
          </div>
          <div className='placement'>
            <label>Tournament Description</label>
            <textarea row={4} required className='form-control' onChange={(event) => this.setState({Desc : event.target.value})} />
          </div>
          <div className='placement'>
            <label>Winning Amount</label>
            <input type='number' required className='form-control' onChange={(event) => this.setState({WinAmount : event.target.value})} />
          </div>
          <div className='placement'>
            <label>Tournament Date</label>
            <input type='date' required className='form-control' onChange={(event) => this.setState({Date : event.target.value})} />
          </div>
          <div className='placement'>
            <label>Tournament Time</label>
            <div className="form-row">
              <div className="col">
                <input type="number" required min={0} max={24} step={1} className="form-control" placeholder="Hours - 24:00 format"  onChange={(event) => this.setState({Hours : event.target.value})} />
              </div>
              <div className="col">
                <input type="number" required min={0} max={59} step={1} className="form-control" placeholder="Minutes" onChange={(event) => this.setState({Minutes : event.target.value})} />
              </div>
            </div>
          </div>
          <div className='placement'>
            <label>S3 Image/Poster Name (jpeg | png | jpg)</label>
            <input type='text' required className='form-control' placeholder="eg : pubg.png" pattern="[^\s]+(\.(jpg|jpeg|png))$" onChange={(event) => this.setState({Poster : event.target.value})} />
          </div>
          <div className='placement'>
            <label>Total Slots</label>
            <input type='number' required className='form-control' onChange={(event) => this.setState({Slots : event.target.value})} />
          </div>
          <div className='placement'>
            <label>Entry Fee</label>
            <input type='number' required className='form-control' onChange={(event) => this.setState({Fee : event.target.value})} />
          </div>
          <div className='placement'>
            <label>Registration Close Interval in Minutes</label>
            <input type='number' required className='form-control' onChange={(event) => this.setState({End : event.target.value})} />
          </div>
          <div className='placement'>
            <input type="submit" disabled={this.state.disable} value="Create" required className='btn btn-info' />
          </div>
        </form>
        {this.state.alert ? (
            <div className="alert alert-success" role="alert" style={{marginTop: '4%'}}>
              Tournament Created Successfully
            </div>
          ) : null}
      </body>
    )
  }
}

export default withRouter(AddTournament);
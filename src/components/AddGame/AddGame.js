import React, { Component } from 'react';
import './style.css';
import {withRouter} from 'react-router-dom';
import config from "../../config";

class AddGame extends Component {
  constructor(props) {
    super(props);
    this.state = {
      GameName: null,
      GameLabel: null,
      PosterURL: null,
      Step1 : null,
      Step2 : null,
      alert: null,
    }
    this.addGame = this.addGame.bind(this);
  }

  addGame() {
    fetch(`${config.url}gameUpload`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        label : this.state.GameLabel, 
        value : this.state.GameName,
        url: this.state.PosterURL,
        steps : {
          step1 : this.state.Step1,
          step2 : this.state.Step2
        }
      }),
    })
    .then(response =>  response.json())
    .then(resData => {
      if(resData.n === 1 && resData.ok === 1) {
        this.setState({alert : true})
        setTimeout(() => this.setState({alert : null}), 3000);
      }
    })
    .catch(err => console.log(err));
  }

  render() {
    return(
      <body className='container' style={{marginTop:100}}>
        <div className='row'>
          <div className='col-lg-6'>
            <h3 style={{fontWeight: '500', color: 'grey'}}>Add a Game</h3>
            <form className='form-group' onSubmit={this.addGame}>
            <div className='placement'>
              <label>Game name</label>
              <input type='text' required className='form-control' onChange={(event) => this.setState({GameName : event.target.value})} />
            </div>
            <div className='placement'>
              <label>Game code</label>
              <input type='text' required className='form-control' onChange={(event) => this.setState({GameLabel : event.target.value})} />
            </div>
            <div className='placement'>
              <label>Game poster url</label>
              <input type='url' required className='form-control' onChange={(event) => this.setState({PosterURL : event.target.value})} />
            </div>
            <div className='placement'>
              <label>Step1 url</label>
              <input type='url' required className='form-control' onChange={(event) => this.setState({Step1 : event.target.value})} />
            </div>
            <div className='placement'>
              <label>Step2 url</label>
              <input type='url' required className='form-control' onChange={(event) => this.setState({Step2 : event.target.value})} />
            </div>
            <div className='placement'>
              <input type='submit' value='Add' required className='btn btn-info' />
            </div>
            </form>
          </div>
          <div className='col-lg-5'>
            {this.state.PosterURL ? (
              <img style={{marginTop: 60}} src={this.state.PosterURL} alt='cover' height='500' width='400'></img>
            ):(
              <div></div>
            )}
          </div>
          {this.state.alert !== null ? (
            <div className="alert alert-success" role="alert" style={{marginTop: '4%'}}>
              Booking Successful
            </div>
          ) : null}
        </div>
      </body>
    )
  }
}

export default withRouter(AddGame);
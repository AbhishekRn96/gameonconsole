import React, { Component } from 'react';
import {withRouter} from 'react-router-dom';
import config from "../../config";
import { Digital } from 'react-activity';
import 'react-activity/dist/react-activity.css';

class ShowPlayers extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data : [],
      loading : true,
      setting : null,
      status : null,
    }
    this.getPlayers = this.getPlayers.bind(this);
    this.addAmount = this.addAmount.bind(this);
    this.addPrize = this.addPrize.bind(this);
  }

  componentDidMount() {
    this.getPlayers();
  }

  addPrize(event, UserId) {
    let arr = this.state.data;
    arr.map((item, key) => {
      if(item.User.UserId === UserId) {
        item.Prize = event.target.value
      }
    })
    this.setState({data : arr});
  }

  getPlayers() {
    this.setState({loading: true, data : []});
    let TournamentData = this.props.location.state;
    fetch(`${config.url}getPlayers`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        TournamentId : TournamentData.TournamentId,
      }),
    })
    .then(response =>  response.json())
    .then(resData => {
      if(resData.length > 0) {
        this.setState({data : resData, loading : false});
      } else {
        this.setState({loading : false});
      }
    })
    .catch(err => {
      console.log(err)
      this.setState({loading : false});
    });
  }

  addAmount(UserId, Amount) {
    this.setState({setting : UserId});
    let TournamentData = this.props.location.state;
    fetch(`${config.url}addAmount`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        TournamentId : TournamentData.TournamentId,
        UserId : UserId,
        Amount : Number(Amount)
      }),
    })
    .then(response =>  response.json())
    .then(resData => {
      if(resData.message === "success") {
        this.setState({status : "success", setting : null}, () => {
          setTimeout(() => this.setState({status : null}), 3000);
        })
      } else {
        this.setState({status : "failed", setting : null}, () => {
          setTimeout(() => this.setState({status : null}), 3000);
        })
      }
    })
    .catch(err => {
      console.log(err);
      this.setState({status : "failed", setting : null}, () => {
        setTimeout(() => this.setState({status : null}), 3000);
      })
    });
  }

  render() {
    const TournamentData = this.props.location.state;
    return (
      <body className='container' style={{marginTop:100}}>
        {this.state.status === "failed" ? (
          <div className="alert alert-danger alert-dismissible fade show" role="alert">
            Error occured while adding Prize Money
            <button type="button" className="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        ) : this.state.status === "success" ? (
          <div className="alert alert-success alert-dismissible fade show" role="alert">
            Prize Money added successfully
            <button type="button" className="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        ) : null}
        <div className='row'>
          <div className='col-md-3'>
            <h5>UserId</h5>
          </div>
          <div className='col-md-3'>
            <h5>Gamer Username</h5>
          </div>
          <div className='col-md-3'>
            <h5>Amount</h5>
          </div>
          <div className='col-md-3'>
          </div>
        </div>
        {this.state.loading ? (
            <center>
              <div style={{marginTop: '20%'}}>
                <Digital color="#1C8ADB" size={50} speed={1} animating={true} />
              </div>
            </center>
          ) : this.state.data.length > 0 ? (
            this.state.data.map((item, key) => (
              <div className='row' key={key} style={{marginBottom: 5, marginTop: '5%'}}>
                <div className='col-md-3'>
                  <h6>{item.User.UserId}</h6>
                </div>
                <div className='col-md-3'>
                  <h6>{item.GameId}</h6>
                </div>
                <div className='col-md-3'>
                  <input type="number" className="form-control" value={item.Prize} placeholder="Enter Amount" onChange={(event) => this.addPrize(event, item.User.UserId)} />
                </div>
                <div className='col-md-3'>
                  {this.state.setting === item.User.UserId ? (
                    <Digital color="#1C8ADB" size={25} speed={1} animating={true} />
                  ) : (
                    <button disabled={item.Prize ? false : true} className='btn btn-info' onClick={() => this.addAmount(item.User.UserId, item.Prize)}>Add</button>
                  )}
                </div>
              </div>
            ))
          ) : (
            <center>
              <div className="text-primary" style={{marginTop: '20%', fontSize: 20, fontWeight: '500'}}>No Tournaments for this Selection Criteria</div>
            </center>
        )}
      </body>
    )
  }
}

export default withRouter(ShowPlayers);
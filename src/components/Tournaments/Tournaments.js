import React, { Component } from 'react';
import './styles.css'
import { Link, withRouter } from 'react-router-dom';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from 'moment';
import config from "../../config";
import { Digital } from 'react-activity';
import 'react-activity/dist/react-activity.css';

class Tournaments extends Component {
  constructor(props) {
    super(props);
    this.state = {
      date : '',
      loading : false,
      data : [],
    }
    this.getTournaments = this.getTournaments.bind(this);
    this.route = this.route.bind(this);
  }

  getTournaments() {
    this.setState({loading: true, data : []});
    fetch(`${config.url}getTournaments`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        date : moment(this.state.date).format().slice(0,10),
      }),
    })
    .then(response =>  response.json())
    .then(resData => {
      if(resData.length > 0) {
        this.setState({data : resData, loading : false});
      } else {
        this.setState({loading : false});
      }
    })
    .catch(err => {
      console.log("fail ",err);
    });
  }

  route(item) {
    this.props.history.push({
      pathname : '/viewTournament',
      state : item
    });
  }

  handleChange(date) {
    this.setState({
      date: date
    });
  }

  render() {
    return (
      <div className="container-fluid" style={{marginTop:110}} >
        <div className="row" style={{marginLeft: '2%'}}>
          <DatePicker
            dateFormat="MMM dd"
            style={{padding: 10}}
            selected={this.state.date}
            onChange={this.handleChange.bind(this)}
            className="form-control"
            placeholderText="Select Date"
            // maxDate={this.state.dates}
            fixedHeight 
          />
          <button className="btn btn-primary" style={{marginLeft: '1%'}} onClick={() => this.getTournaments()} disabled={this.state.date ? false : true}>
            Go
          </button>
          <div className="col-sm-1 offset-6">
            <button className="btn btn-primary" style={{color: 'white', fontWeight: '500'}} onClick={() => this.props.history.push('addTournament')}>
              Create Tournament
            </button>
          </div>
        </div>
        <body>
          {this.state.loading ? (
              <center>
                <div style={{marginTop: '20%'}}>
                  <Digital color="#1C8ADB" size={50} speed={1} animating={true} />
                </div>
              </center>
            ) : this.state.data.length > 0 ? (
              this.state.data.map((item, key) => (
                <div className='card shadow col-md-12' key={key} style={{marginBottom: 5, marginLeft: 10, marginRight: 5, marginTop: '5%'}} onClick={() => this.route(item)}>
                  <div className='card-body col-md-12'>
                    <div className='row'>
                      <img 
                        src={item.Poster}
                        alt='main'
                        style={{height: 100, marginLeft: 10, borderRadius: 3}}/>
                      <div style={{marginLeft : 12}}>
                        <h6 className='heading'>{item.Name} ({item.TournamentId})</h6>
                        <span style={{color : 'grey', fontWeight: '500', fontSize: 18}}>
                          <span>
                            <i className='material-icons' style={{color: '#4e6387', marginRight: '0.5%'}}>query_builder</i>
                          </span>
                          {item.Hours+':'+item.Minutes}
                        </span><br />
                        <span>Slots - {item.Slots}</span><br />
                        <span style={{color: 'green'}}>Fee - &#8377;{item.Fee}</span><br />
                        <span>Starting Interval - {item.End} Minutes</span>
                      </div>
                    </div>
                  </div>
                </div>
              ))
            ) : (
              <center>
                <div className="text-primary" style={{marginTop: '20%', fontSize: 20, fontWeight: '500'}}>No Tournaments for this Selection Criteria</div>
              </center>
          )}
        </body>
      </div>
    )
  }
}

export default withRouter(Tournaments);
import React, { Component } from 'react';
import { Switch, withRouter, Route} from 'react-router-dom';
import { Redirect } from 'react-router'
import Home from '../Home/Home'
import AddCafe from '../AddCafe/AddCafe';
import Cafes from '../Cafes/Cafes';
import Bookings from "../Bookings/Bookings";
import AddGame from '../AddGame/AddGame';
import EditCafe from '../EditCafe/EditCafe';
import ViewCafe from '../ViewCafe/ViewCafe';
import ViewDates from '../ViewDates/ViewDates';
import Tournaments from '../Tournaments/Tournaments';
import AddTournament from '../AddTournament/AddTournament';
import ViewTournament from '../ViewTournament/ViewTournament';
import EditTournament from '../EditTournament/EditTournament';
import ShowPlayers from '../ShowPlayers/ShowPlayers';
import Notification from '../Notification/Notification';

class Routes extends Component {
  render() {
    return(
      <Switch>
        <Route exact path='/' component={Home}></Route>
        <Route exact path='/addCafe' render={() => sessionStorage.getItem('user') ? <AddCafe /> : <Redirect to="/"/>}></Route>
        <Route exact path='/cafes' render={() => sessionStorage.getItem('user') ? <Cafes /> : <Redirect to="/"/>}></Route>
        <Route exact path='/bookings' render={() => sessionStorage.getItem('user') ? <Bookings /> : <Redirect to="/"/>}></Route>
        <Route exact path='/addGame' render={() => sessionStorage.getItem('user') ? <AddGame /> : <Redirect to="/"/>}></Route>
        <Route exact path='/editCafe' render={() => sessionStorage.getItem('user') ? <EditCafe /> : <Redirect to="/"/>}></Route>
        <Route exact path='/viewCafe' render={() => sessionStorage.getItem('user') ? <ViewCafe /> : <Redirect to="/"/>}></Route>
        <Route exact path='/viewDates' render={() => sessionStorage.getItem('user') ? <ViewDates /> : <Redirect to="/"/>}></Route>
        <Route exact path='/tournaments' render={() => sessionStorage.getItem('user') ? <Tournaments /> : <Redirect to="/"/>}></Route>
        <Route exact path='/addTournament' render={() => sessionStorage.getItem('user') ? <AddTournament /> : <Redirect to="/"/>}></Route>
        <Route exact path='/viewTournament' render={() => sessionStorage.getItem('user') ? <ViewTournament /> : <Redirect to="/"/>}></Route>
        <Route exact path='/editTournament' render={() => sessionStorage.getItem('user') ? <EditTournament /> : <Redirect to="/"/>}></Route>
        <Route exact path='/showPlayers' render={() => sessionStorage.getItem('user') ? <ShowPlayers /> : <Redirect to="/"/>}></Route>
        <Route exact path='/notification' render={() => sessionStorage.getItem('user') ? <Notification /> : <Redirect to="/"/>}></Route>
      </Switch>
    )
  }
}

export default withRouter(Routes);
import React, { Component } from 'react';
import './style.css'
import { Link, withRouter } from 'react-router-dom';

class Header extends Component {
  constructor(props) {
    super(props);
    this.Logout = this.Logout.bind(this);
  }

  Logout() {
    sessionStorage.clear();
    this.props.history.push('/');
  }

  render() {
    return (
      <div>
        <nav className="navbar navbar-expand-sm bg-light navbar-light shadow fixed-top">
          <div className='container'>
            <Link className="navbar-brand" to='/cafes'>
              <img src={require('../../images/icon.png')} alt="logo" style={{width: 180}}/>
            </Link>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              {sessionStorage.getItem('user') ? (
                <ul className="navbar-nav">
                  <li className='nav-item'>
                    <Link className='nav-link' style={{fontSize : 17, fontWeight: '500'}} to='/cafes'>Cafes</Link>
                  </li>
                  <li className='nav-item'>
                    <Link className='nav-link' style={{fontSize : 17, fontWeight: '500'}} to='/bookings'>Bookings</Link>
                  </li>
                  <li className='nav-item'>
                    <Link className='nav-link' style={{fontSize : 17, fontWeight: '500'}} to='/tournaments'>Tournaments</Link>
                  </li>
                </ul>
              ) : (
                null
              )}
              {sessionStorage.getItem('user') ? (
                <ul className="navbar-nav ml-auto">
                  <li className='nav-item'>
                    <li className='nav-item dropdown'>
                      <Link className='nav-link dropdown-toggle' id='navbardrop' to='#' style={{fontSize: 17, fontWeight: '500'}} data-toggle='dropdown'>{sessionStorage.getItem('user')}</Link>
                      <div className='dropdown-menu'>
                        <Link className='dropdown-item' style={{fontSize : 17, fontWeight: '500'}} to='/addCafe'>Add a Cafe</Link>
                        <Link className='dropdown-item' style={{fontSize : 17, fontWeight: '500'}} to='/addGame'>Add a Game</Link>
                        <Link className='dropdown-item' style={{fontSize : 17, fontWeight: '500'}} to='/notification'>Notification</Link>
                        <div class="dropdown-divider"></div>
                        <Link className='dropdown-item' style={{fontSize: 17, fontWeight: '500'}} to='#' onClick={this.Logout}>Log Out</Link>
                      </div>
                    </li>
                  </li>
                </ul>
              ) : (
                null
              )}
            </div>
          </div>
        </nav>
      </div>
    )
  }
}

export default withRouter(Header);
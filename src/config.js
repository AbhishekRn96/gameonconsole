const mode = "prod"

const config = {
  url : mode == "dev" ? "http://localhost:3020/" : "https://gameonadmincontrol.herokuapp.com/"
}

export default config;